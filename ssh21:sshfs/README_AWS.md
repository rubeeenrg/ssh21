# SSH
## Rubén Rodríguez ASIX M06-ASO 2021-2022

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona

### Documentació:
El primer que hem fet ha sigut crear la màquina EC2 a AWS. Hem generat les claus (només utilitzarem la privada ja que AWS ja té la pública, i quan ens connectem, sens grabarà la pública)

Després ens hem copiat la clau de 'root' del container 'pam.edt.org' al usuari 'unix01' del container 'sshfs.edt.org' dins de AWS.

Fem la següent ordre per iniciar els containers 'ldap.edt.org' i 'sshfs.edt.org'.

```
docker compose up -d
```

Comprobem que tenim bé el LDAP: ldapsearch -x -h localhost -LLL -b 'dc=edt,dc=org'
Comprobem que tenim bé la connexió SSH amb els usuaris unix per el port 2022: ssh -p 2022 unix01@localhost
Comprobem que tenim bé la connexió SSH amb els usuaris LDAP per el port 2022: ssh -p 2022 unix01@localhost

Des de fora, comprobem que podem fer consultes LDAP: ldapsearch -x -h 'IP_AMAZON' -LLL -b 'dc=edt,dc=org' 

Generem la clau de 'root' i la copiem i la possem al 'authorized_keys' de 'unix01'

```
ssh-keygen -O root
ssh unix01@34.235.254.220 -p 2022
```
