# SSH
## Rubén Rodríguez ASIX M06-ASO 2021-2022

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona

### Documentació:
#### Pràctica 16:
Primer de tot, hem iniciat una màquina 'Windows Server 2022' a AWS. A continuació, hem instal·lat 'remmina' al nostre servidor. A continuació, hem geenrat un password amb la nostra clau privada d'AWS.
Anem a 'Actions --> Security --> Generate Windows Password --> Browse (Escollim la nostra clau, que la mostrava amb encriptació) --> Prenem el botó de sota i desencriptem la clau.'
Engeguem 'Remmina', li donem al '+' d'adalt a l'esquerra, i fem el següent:

```
Server: <IP pública AWS>
Username: Administrator
Password: <Password generat en funció a la clau privada d'AWS>
```
Després prenem 'connect' i ja està.

#### Instal·lació Remmina:
```
echo 'deb http://ftp.debian.org/debian stretch-backports main' | sudo tee --append /etc/apt/sources.list.d/stretch-backports.list >> /dev/null
sudo apt-get update
sudo apt-get install remmina remmina-plugin-*
```

```
docker compose up -d
```
