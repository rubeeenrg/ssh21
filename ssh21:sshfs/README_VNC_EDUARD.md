Iniciem el servei des de el servidor:

```
vncserver
```

Fem un túnel SSH per connectar-nos remotament però utilitzant connexió 'localhost':

```
ssh -fN -L 5901:localhost:5901 pere@i21 --> túnel SSH --> 5901 (1) port del nostre ordinador, localhost perquè reboti amb localhost del i21, al port 5901(2) destí, com a usuari pere de la màquina en qüestió.
```

Fem la connexió:

```
vncviewer localhost:1
```

Copiem el següent fitxer a /etc/, ja que /etc/ té preferència de configuració:

```
cp /lib/systemd/system/tigervncserver@.service /etc/systemd/system/vncserver-pere@.service
```
