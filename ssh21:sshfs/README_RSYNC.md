# SSH
## Rubén Rodríguez ASIX M06-ASO 2021-2022

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona

### Documentació:
Primer ens connectem al nostre servidor (i21) i habilitem l'ssh per poder treballar remotament, després, instal·lem 'rsync'.
A continuació ens connectem des de el nostre ordinador (client) al servidor (i21).

```
sudo ssh isx48062351@i21
apt install rsync
```

#### Pràctica 17:
##### APARTAT 1:
##### 1- Copiar de 'local a local':
Probarem de copiar un arxiu dins d'un altre directori i a copiar un directori dins d'un altre directori canviant el nom d'aquest.

```
Creem els arxius i directoris...
mkdir carpeta1 carpeta2
touch carpeta1/exemple1 carpeta1/exemple2

rsync carpeta1/exemple1 carpeta2/
rsync -r carpeta1 carpeta2/carpetilla
```

##### 2- Copiar de 'local a remot':
Probarem de copiar un arxiu desde el nostre client al servidor (i21).

```
rsync -a ~/carpeta_carpeton isx48062351@i21:~/carpeta_vuelta
```

##### 3- Copiar de 'remot a local':
Probarem de copiar un arxiu desde el servidor (i21) dins el nostre ordinador (client) al nostre 'home'.

```
rsync -a isx48062351@i21:~/carpeta2 ~/carpeta_carpeton
```

##### APARTAT 2:
##### 1- Permetre accés a usuaris en concret:
Generarem tres recursos rsync, cofngiurarem els recursos de manera que alguns siguin de 'rw' i altres de 'ro' i configurarem l'accés a certs usuaris.
Generarem un arxiu en el servidor (i21) que es diurà '/etc/rsyncd.conf', dins d'aquest arxiu, possarem els paràmetres corresponents a aquests.
A continuació, desde el client, farem el següent:

```
rsync -ap <IP_SERVIDOR>:: --> ACTIVEM EL MODUL QUE VOLEM COMPARTIR 
rsync -rtv <user_compartit>@<host/IP_SERVIDOR>::<nom_modul_compartit(epoptes --> fitxer rsyncd.conf)>/<path_que_volem_compartir(*)> <path_on_volem_compartir-lo>(.)
```
