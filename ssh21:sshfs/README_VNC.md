# SSH
## Rubén Rodríguez ASIX M06-ASO 2021-2022

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona

### Documentació:
Dins del servidor, instal·lem les següents dependències:

```
apt install tigervnc-standalone-server				        (software SERVIDOR)
apt install tigervnc-common		         		        (software CLIENT)
apt install xfce4 xfce4-goodies xorg dbus-x11 x11-xserver-utils		(plugins per escritoris LINUX)
```

Executem la següent ordre, possem el password (x2), i possem 'no' a la opció 'ro':

```
vncserver
```

A continuació, hem de carregar-nos el servei (:1) per poder configurar.

```
vncserver -kill :1
```

### Configuració servidor:
Creem el següent fitxer i li possem permisos d'execució a l'usuari:

vim ~/.vnc/xstartup
```
#!/bin/sh
xrdb $HOME/.Xresources
startxfce4 &

```
chmod 777 ~/.vnc/xstartup

Executem la següent ordre dins del directori '.vnc' per possar tot en marxa i comprobem:

```
tigervncserver -xstartup /usr/bin/xterm		(Enjega i obra el port corresponent, en aquest cas el 5901)
vncserver -list
```

Ens connectem vía 'SSH' al nostre ordinador server (i21):

```
ssh -X root@i21
```

Instal·lem 'VNC-Viewer':

```
wget https://www.realvnc.com/download/file/viewer.files/VNC-Viewer-6.21.406-Linux-x64.deb
dpkg -i VNC-Viewer-6.21.406-Linux-x64.deb
```

### Ordres client:
Ens connectem vía 'SSH' al nostre ordinador server (i21):

```
ssh -X root@i21
```

Executem 'vncviewer':

```
vncviewer localhost:5901
```

### Fitxer de servei 'vncserver@.service' (INACABAT):
Anem a '/etc/systemd/system', creem el següent arxiu i possem la següent informació dins:

vim vncserver@.service
```
[Unit]
Description=Start TigerVNC server at startup
After=syslog.target network.target

[Service]
Type=forking
User=guest
Group=guest
WorkingDirectory=/home/guest

PIDFile=/home/guest/.vnc/%H:%i.pid
ExecStartPre=-/usr/bin/vncserver -kill :%i > /dev/null 2>&1
ExecStart=tigervncserver -xstartup /usr/bin/xterm -depth 24 -geometry 1280x800 :%i
ExecStop=/usr/bin/vncserver -kill :%i

[Install]
WantedBy=multi-user.target
```

Recarreguem el 'daemon', iniciem el servei i comprobem l'status:

```
systemctl daemon-reload
systemctl start vncserver@1
systemctl status vncserver@1
```
