# SSH
## Rubén Rodríguez ASIX M06-ASO 2021-2022

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona

### SSH Containers:

 * **rubeeenrg/ssh21:base** Host base amb el *servei SSH* engegat. El host està configurat 
  amb PAM per permetre usuaris locals (unix01, etc) i usuaris de LDAP (pere,...). Cal
  engegar el container conjuntament amb el servei LDAP.

### Configuració:
A l'arxiu '/etc/sshd/sshd_config' habilitem l'autentificació per clau pública i per password, tot i que la de password la treurem més endevant: "PasswordAuthentication yes" i "PubkeyAuthentication yes"

Iniciarem el servei de servidor a l'arrancar: "service ssh start" --> arranca. 

Ídem --> "/usr/sbin/sshd"

#### 1era de 3 formes de connexió amb autentificació d'usuari, (EL FINGER PRINT EL FA SEMPRE !!!)
Per password:

ssh unix01@172.19.0.2

The authenticity of host '172.18.0.2 (172.18.0.2)' can't be established. ECDSA key fingerprint is SHA256:iLiNaZkOQrgzyDNHOFfyonT052e46LhydcrcnxA65Qo. Are you sure you want to continue connecting (yes/no/[fingerprint])? yes

Warning: Permanently added '172.18.0.2' (ECDSA) to the list of known hosts. ssh_dispatch_run_fatal: Connection to 172.18.0.2 port 22: Broken pipe (el missatge d'error era pq no hem propagat el port 22 en la xarxa docker)

Al aceptar, estem afegint al nostre arxiu (local) ~/.ssh/known_hosts, la relació fingerprint (hash CLAU PÚBLICA maq remota) -IP maq remota (per seguretat) per si la IP cambia que no ens conectem on no toca. (és la Verificació del host remot)

Ara pregunta password i dins... (provat amb usuaris UNIX i LDAP)

sshd_config cambiar: "PasswordAuthentication yes" i "PubkeyAuthentication no"

### 1b) Per parelles de claus publica/privada

Un compte d'usuari en el servidor remot (ex: unix01) confiarà amb tots els usuaris d'un host client si disposa en el seu "authorized_keys" de la clau pública del host client. Per tant cal concatenar la clau "/etc/ssh/ssh_host_key.pub" (o la RSA o DSA) al "~/.ssh/authorized_keys" del compte de l'usuari en el servidor remot.

sshd_config cambiar: "PasswordA no" i "PubkeyAuthen yes"

Hem d'enviar a l'usuari del servidor (unix01) les claus de l'usuari client (isx48062351) que executi el ssh ubicades a "\~/.ssh/NOM_X__rsa.pub" cap a "~/.ssh/authorized_keys"

ssh-copy-id  usuari@IP_servidor

scp arxiu.txt usuari@domini.com:/home/usuario (HABILITEM ACCÉS PER PASSWORD I HO FEM)

### Configuració servidor:
Canviem el port del SSH a un escollit per nosaltres (hem de tenir en compte que no estigui en el rang de ports reservats), ens fiquem al fitxer "/etc/ssh/sshd_config" i descomentem la línea de "Port" i possem el port que haguem escollit.

Descomentem la línea de "PermitRootLogin" i li afegim la opció "no" perquè no ens permeti entrar com a root.

AllowUsers --> permetem que usuaris en concrets siguin només els que puguin accedir.

DenyUsers --> excluim als usuaris concrets que no volem que puguin entrar.

Pàgina per ajuda amb els paràmetres de configuració de servidor:

https://www.redeszone.net/tutoriales/servidores/servidor-openssh-linux-configuracion-maxima-seguridad/

### Iniciació amb docker-compose
https://docs.docker.com/compose/install/

https://docs.docker.com/compose/cli-command/

``` 
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d rubeeenrg/ldap21:groups
docker run --rm --name ssh.edt.org -h ssh.edt.prg --net 2hisix --privileged -p 2022:22 -d rubeeenrg/ssh21:base
```

```
docker compose up -d
ssh unix01@localhost -p 2022
docker compose down
```
